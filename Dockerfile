FROM python:3

ENV VAULT_ADDR https://vault-systems.oit.duke.edu
ENV DEBIAN_FRONTEND 'noninteractive'

SHELL ["/bin/bash", "-eo", "pipefail", "-c"]

# hadolint ignore=DL3008,DL3005
RUN apt-get update && \
    apt-get install -y --no-install-recommends \
      software-properties-common lsb-release && \
    curl -fsSL https://apt.releases.hashicorp.com/gpg | apt-key add - && \
    apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main" && \
    apt-get update && \
    apt-get -y install --no-install-recommends vault && \
    rm -rf /var/lib/apt/lists/*
